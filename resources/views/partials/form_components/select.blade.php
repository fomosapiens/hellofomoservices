<div class="component-wrapper">
    <label for="{{ \App\Helpers\Css::makeId($component_data['name']) }}">{{ $component_data['name'] }}</label>
    <select name="{{ \App\Helpers\Css::makeName($component_data['name']) }}" id="{{ \App\Helpers\Css::makeId($component_data['name']) }}">
        @foreach($component_data['options'] as $option)
            <option value="{{ $option['name'] }}">{{ $option['name'] }}</option>
        @endforeach
    </select>
</div>
