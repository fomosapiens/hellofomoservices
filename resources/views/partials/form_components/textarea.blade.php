<div class="component-wrapper">
    <label for="{{ \App\Helpers\Css::makeId($component_data['name']) }}">{{ $component_data['name'] }}</label>
    <textarea name="{{ \App\Helpers\Css::makeName($component_data['name']) }}" id="{{ \App\Helpers\Css::makeId($component_data['name']) }}" rows="6"></textarea>
</div>
