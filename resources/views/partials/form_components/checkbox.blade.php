<div class="component-wrapper">
    <label for="{{ \App\Helpers\Css::makeId($component_data['name']) }}">
        <input
                type="checkbox"
                value="(bestätigt)"
                id="{{ \App\Helpers\Css::makeId($component_data['name']) }}"
                name="{{ \App\Helpers\Css::makeName($component_data['name']) }}"
                {!! (isset($component_data['attributes']))? \App\Helpers\Css::printAttributes($component_data['attributes']) : '' !!}
                required
        >
    {{ $component_data['name'] }}</label>
</div>
