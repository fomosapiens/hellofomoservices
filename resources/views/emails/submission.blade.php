<div>
@foreach ($submitted_data as $field)

    @php
        $index = $field['index'];
    @endphp

    @if(!isset($component_data[$index]['visibility']) || $component_data[$index]['visibility'] == 'mail')
        @if(in_array($field['type'], ['text', 'date']))
            <p><strong>{{ $field['name'] }}:</strong> {{ $field['value'] }}</p>
        @elseif(in_array($field['type'], ['checkbox']))
            <p><strong>{{ ($field['value'] == false)? '(nicht bestätigt)' : $field['value'] }}</strong> {{ $field['name'] }}</p>
        @elseif(in_array($field['type'], ['paragraph']))
            <p>{!! $field['content'] !!}</p>
        @endif
    @endif
@endforeach
</div>
