<?php

return (object) [
    'settings' => [
        'accent_color' => '#494',
        'theme' => 'dark',
        'mailto' => 'schulz@rgc-manager.de',
        'css' => '',
        'head' => '',
        'header' => '<h1>Same Form, different Client</h1>',
        'footer' => ''
    ]
];
