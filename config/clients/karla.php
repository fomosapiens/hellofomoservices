<?php

return (object) [
    'settings' => [
        'accent_color' => '#8C6F74',
        'theme' => 'light',
        'mailto' => 'karlas-kaffeekrams@outlook.de',
    ]
];
