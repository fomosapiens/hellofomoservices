<?php

return (object) [
    'components' => [
        [
            'type' => 'paragraph',
            'content' => '<strong>Erklärung zur Gäste-Registrierung</strong>'
        ],
        [
            'type' => 'paragraph',
            'content' => 'Coronabedingt dürfen wir nur unter strengen Auflagen unseren Service anbieten. Dazu gehört, dass wir Ihre Kontaktdaten erfassen müssen. Wir bitten Sie, uns folgende Angaben zuzusenden:'
        ],
        [
            'type' => 'text',
            'name' => 'Vorname',
            'attributes' => [
                'required' => 'required'
            ]
        ],
        [
            'type' => 'text',
            'name' => 'Nachname',
        ],
        [
            'type' => 'text',
            'name' => 'Straße',
        ],
        [
            'type' => 'text',
            'name' => 'PLZ + Wohnort',
        ],
        [
            'type' => 'text',
            'name' => 'E-Mail',
        ],
        [
            'type' => 'text',
            'name' => 'Telefonnummer',
        ],
        [
            'type' => 'date',
            'name' => 'Ich habe eine Reservierung vorgenommen für den:',
        ],
        [
            'type' => 'paragraph',
            'content' => '<strong>Mit Versendung dieses Dokuments versichere ich, dass...</strong>'
        ],
        [
            'name' => '...ich nicht an Covid19-typischen Krankheitssymptomen (v.a. Fieber, Husten und Atemnot, Geschmacks- und Geruchsstörungen) leide, außerdem versichere ich, dass ich nicht unter Quarantäne gestellt bin.',
            'type' => 'checkbox'
        ],
        [
            'name' => '...die Personen meiner Reservierung aus nicht mehr als 2 Haushalten stammen.',
            'type' => 'checkbox'
        ],
        [
            'name' => '...ich die folgenden datenschutzrechtlichen Hinweise zur Kenntnis genommen habe und hiermit einverstanden bin.',
            'type' => 'checkbox'
        ],
        [
            'type' => 'paragraph',
            'content' => 'Datenschutzrechtliche Hinweise: Die Verarbeitung Ihrer Daten erfolgt auf Grundlage von Art. 6 Abs. 1 c), d), e) DSGVO und Art. 9 Abs. 2 DSGVO. Die Verarbeitung ist erforderlich, um Beschäftigte im laufenden Betrieb zu schützen und ggf. Kontaktpersonen von an Covid-19 erkrankten Personen ausfindig zu machen und rechtzeitig verständigen zu können. Somit dient die Verarbeitung Ihrer Gesundheit und der Ihrer Mitmenschen. Ihre Daten werden erforderlichenfalls an das zuständige Gesundheitsamt weitergegeben. Ihre Daten werden gelöscht, sobald sie für ihre Erhebung nicht mehr erforderlich sind. Sie haben das Recht auf Auskunft über die bei uns gespeicherten Daten. Sie haben das Recht auf Berichtigung, Löschung und Einschränkung der Verarbeitung unter den in Art. 16 bis 18 DSGVO genannten Voraussetzungen. Ihnen steht ein Beschwerderecht bei der Aufsichtsbehörde zu.'
        ],
        [
            'type' => 'paragraph',
            'content' => '<strong>Diese Daten werden wir bei Ihrem Besuch erfassen:</strong><br><ul><li>Ankunft im Café</li><li>Verlassen des Cafés</li></ul>',
            'visibility' => 'form'
        ],
        [
            'type' => 'paragraph',
            'content' => 'Ankunft im Café:',
            'visibility' => 'mail'
        ],
        [
            'type' => 'paragraph',
            'content' => 'Verlassen des Cafés:',
            'visibility' => 'mail'
        ],
        [
            'type' => 'paragraph',
            'content' => 'Name Service:',
            'visibility' => 'mail'
        ],
        [
            'name' => 'Versenden',
            'type' => 'submit'
        ]
    ]
];
