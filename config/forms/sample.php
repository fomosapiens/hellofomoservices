<?php

return (object) [
    'settings' => [
        'accent_color' => '#0066cc',
        'mailto' => 'noni-junk@outlook.de',
        'css' => '',
        'header' => '',
        'footer' => ''
    ],
    'components' => [
        [
            'type' => 'paragraph',
            'content' => 'Dies ist ein Blocktext.',
        ],
        [
            'name' => 'Vorname',
            'type' => 'text',
        ],
        [
            'name' => 'Nachname',
            'type' => 'text',
        ],
        [
            'name' => 'Geschlecht',
            'type' => 'select',
            'options' => [
                [
                    'name' => 'männlich'
                ],
                [
                    'name' => 'weiblich'
                ],
                [
                    'name' => 'anderes'
                ]
            ],
        ],
        [
            'name' => 'Diabetiker',
            'type' => 'radio',
            'options' => [
                [
                    'name' => 'ja'
                ],
                [
                    'name' => 'nein'
                ]
            ],
        ],
        [
            'name' => 'Notizen',
            'type' => 'textarea'
        ],
        [
            'name' => 'AGBs akzeptiert',
            'type' => 'checkbox'
        ],
        [
            'name' => 'Senden',
            'type' => 'submit'
        ]
    ]
];
