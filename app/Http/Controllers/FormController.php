<?php

namespace App\Http\Controllers;

use App\Helpers\Css;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Mail;
use Illuminate\View\View;

class FormController
{
    public function view(string $client, string $form): View
    {
        $client_config_path = "clients.{$client}";
        $form_config_path = "forms.{$form}";

        if (config()->has($client_config_path) && config()->has($form_config_path)) {
            $client_data = config()->get($client_config_path);
            $form_data = config()->get($form_config_path);

            $settings = array_replace($client_data->settings, $form_data->settings ?? []);
            $settings['hellofomo_client'] = $client;
            $settings['hellofomo_form'] = $form;

            $components = $form_data->components;
            return view('form')->with('data',
                (object) [
                    'settings' => $settings,
                    'components' => $components
                ]
            );
        } else {
            return view('welcome');
        }
    }

    public function submit(): RedirectResponse
    {
        $client_config_path = "clients." . request()->hellofomo_client;
        $form_config_path = "forms." . request()->hellofomo_form;

        if (!config()->has($client_config_path) || !config()->has($form_config_path)) {
            session()->flash('message', [
                'error', 'Wir konnten die Nachricht keinem Formular zuordnen, die Daten wurden nicht übermittelt.'
            ]);
            return redirect()->back();
        }

        $client_data = config()->get($client_config_path);
        $form_data = config()->get($form_config_path);

        $components = $form_data->components;
        $submitted_data = [];
        $fields = (array) request()->all();
        for ($i = 0; $i < count($components); $i++) {

            //if(isset($components[$i]['name'])) {

                if(in_array($components[$i]['type'], ['text', 'date'])) {
                    $name = Css::makeName($components[$i]['name']);
                    $submitted_data[] = [
                        'index' => $i,
                        'name' => $components[$i]['name'],
                        'type' => $components[$i]['type'],
                        'value' => $fields[$name]
                    ];

                } elseif (in_array($components[$i]['type'], ['checkbox'])) {
                    $name = Css::makeName($components[$i]['name']);
                    $submitted_data[] = [
                        'index' => $i,
                        'name' => $components[$i]['name'],
                        'type' => $components[$i]['type'],
                        'value' => $fields[$name] ?? false
                    ];

                } elseif (in_array($components[$i]['type'], ['paragraph'])) {
                    $submitted_data[] = [
                        'index' => $i,
                        'type' => $components[$i]['type'],
                        'content' => $components[$i]['content']
                    ];
                }
            //}
        }

        try {
            Mail::send('emails.submission', ['submitted_data' => $submitted_data, 'component_data' => $components], function ($message) use(&$client_data) {
                $message->to($client_data->settings['mailto'])->subject('Formular');
            });
        } catch (\Swift_SwiftException $e) {
            session()->flash('message', [
                'error', 'Bei der Übermittlung des Formulars ist ein Fehler aufgetreten'
            ]);
            return redirect()->back();
        }

        session()->flash('message', [
            'success', 'Das Formular wurde erfolgreich übermittelt'
        ]);
        return redirect()->back();
    }
}
