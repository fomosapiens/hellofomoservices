<?php


namespace App\Helpers;


class Css
{
    private static function encode(string $string): string
    {
        // return bin2hex($string);
        return bin2hex(substr($string, 0, ((strlen($string) > 50)? 50 : strlen($string))));
    }

    private static function decode(string $string): string
    {
        return hex2bin($string);
    }

    private static function removePrefix(string $string): string
    {
        return substr($string, 3);
    }

    public static function makeClass(string $name): string
    {
        return "cl-" . self::encode($name);
    }

    public static function makeName(string $name): string
    {
        return "na-" . self::encode($name);
    }

    public static function makeId(string $name): string
    {
        return "id-" . self::encode($name);
    }

    public static function decodeClass(string $class): string
    {
        return self::decode(self::removePrefix($class));
    }

    public static function decodeName(string $name): string
    {
        return self::decode(self::removePrefix($name));
    }

    public static function decodeId(string $id): string
    {
        return self::decode(self::removePrefix($id));
    }

    public static function printAttributes(array $attributes): string
    {
        $attributes_html = '';
        foreach ($attributes as $attribute => $value) {
            $attributes_html .= ' ' . $attribute . '="' . $value . '"';
        }
        return $attributes_html;
    }
}
